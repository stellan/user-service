// A simple in-memory implementation of a key-value store
// using a map data structure for storage and lookups.

var content = new Map();
//var mongodburl = "mongodb://" + mongodb_host + ":27017/";
var mongo = require('mongodb');
var dbClient = require('mongodb').MongoClient;

//startDbClient();

// Resilience function that retries the database connection every
// five seconds until connected.
function startDbClient() {
  dbClient.connect(mongodburl, function(err, db) {
      if (err) {
          console.error(" [DB] ", err.message);
          return setTimeout(startDbClient, 5000);
      }
      console.log(" [DB] Connected")
      conn = db.db("profile");
      conn.createCollection("profiles", function(err, res) {
          if (err) {
              console.log(err);
          }
      });
  });
}

module.exports.add = function(id, value) {
  content.set(id, value);
}

module.exports.read = function(id) {
  return content.get(id);
}

module.exports.update = function(id, value) {
  content.set(id, value);
}

module.exports.remove = function(id) {
  content.delete(id);
}

module.exports.readAll = function() {
  return content.values();
}