"use strict";

var mongo = require('mongodb');
var dbClient = require('mongodb').MongoClient;

function SimpleMDB(host = "localhost", port = 27017, name = "test", collection = "test") {
  this.host = host;
  this.port = port;
  this.name = name;
  this.collection = collection;
  this.dburl = "mongodb://" + this.host + ":" + this.port + "/" + this.name;
  this.conn;
  this.db;
}

SimpleMDB.prototype.connect = function () {
  return new Promise((resolve, reject) => {
    startDbClient(this, () => {
      resolve();
    });
  });
}

SimpleMDB.prototype.loadOne = function(id) {
  return this.conn.collection(this.collection).findOne({ '_id': id});
}

SimpleMDB.prototype.load = function (query, sortby) {
  return this.conn.collection(this.collection).find(query).sort(sortby).toArray();
}

SimpleMDB.prototype.insert = function (object) {
  return this.conn.collection(this.collection).insertOne(object);
}

SimpleMDB.prototype.insertList = function (array) {
  return this.conn.collection(this.collection).insertMany(array);
}

SimpleMDB.prototype.save = function (data) {
  return this.conn.collection(this.collection).save(data);
}

SimpleMDB.prototype.remove = function (id) {
  return this.conn.collection(this.collection).deleteOne({ '_id': id });
}

SimpleMDB.prototype.clear = function () {
  return this.conn.collection(this.collection).drop();
}

SimpleMDB.prototype.disconnect = function () {
  return this.db.close();
}

// Resilience function that retries the database connection every
// five seconds until connected.
function startDbClient(instance, callback) {
  console.log("collection", instance.collection);
  dbClient.connect(instance.dburl, function (err, db) {
    if (err) {
      console.error(" [DB] ", err.message);
      return setTimeout(startDbClient(instance, callback), 5000);
    }
    console.log(" [DB] Connected")
    instance.db = db;
    instance.conn = db.db(instance.name);
    instance.conn.createCollection(instance.collection, function (err, res) {
      if (err) {
        console.log(err);
      }
      else {
        callback();
      }
    });
  });
}

module.exports = SimpleMDB;