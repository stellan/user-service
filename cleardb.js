var SimpleMDB = require('./SimpleMDB');
var db_host = process.env.db_host || "localhost";
var db_port = process.env.db_host || 27017;
var profileDb = new SimpleMDB(db_host, db_port, "user", "profile");
var passwordDb = new SimpleMDB(db_host, db_port, "user", "credentials");

profileDb
    .connect()
    .then(() => {profileDb.clear()})
    .then(() => {profileDb.disconnect()})
    .catch((err) => {console.log("Error", err)});
passwordDb
    .connect()
    .then(() => {passwordDb.clear()})
    .then(() => {passwordDb.disconnect()})
    .catch((err) => {console.log("Error", err)});
