'use strict';

var express = require('express'),
  router = express.Router(),
  bodyParser = require('body-parser'),
  swaggerUi = require('swagger-ui-express'),
  swaggerDocument = require('./swagger.json');

var app = express();
var SimpleMDB = require('./SimpleMDB');
var db_host = process.env.db_host || "localhost";
var db_port = process.env.db_port || 27017;
var profileDb = new SimpleMDB(db_host, db_port, "user", "profile");
var passwordDb = new SimpleMDB(db_host, db_port, "user", "credentials");

var crypto = require('crypto');

var jwt = require('jsonwebtoken');
var fs = require('fs');

// sign with RSA SHA256
var prvk = fs.readFileSync('private_key.pem');  // get private key
var pubk = fs.readFileSync('public_key.pem');

function sign(payload) {
  return new Promise((resolve, reject) => {
    jwt.sign(payload, prvk, { algorithm: 'RS256' }, (err, token) => {
      if (err) {
        reject(err);
      }
      else {
        console.log("--- BEGIN RSA SHA-256 JSON WEB TOKEN ---");
        console.log(token);
        console.log("---  END RSA SHA-256 JSON WEB TOKEN  ---");
        resolve(token);
      }
    });
  });
}

function verify(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, pubk, function (err, decoded) {
      if (err) {
        reject(err);
      }
      else {
        console.log("Decoded", decoded);
        resolve(decoded);
      }
    });
  });
}


//rest API requirements
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

//middleware for create
var createUser = function (req, res, next) {
  var user = req.body;
  user._id = user.id;
  delete user.id;
  //console.log("User", user);
  profileDb
    .insert(user)
    .then(() => {
      user.id = user._id;
      delete user._id;
      res.json(user);
    })
    .catch((err) => {
      res.status(500);
      res.send(err);
    });
}

var updateUser = function (req, res, next) {
  var user = req.body;
  user._id = user.id;
  delete user.id;
  profileDb
    .save(user)
    .then(() => {
      user.id = user._id;
      delete user._id;
      res.json(user);
    })
    .catch((err) => {
      res.status(500);
      res.send(err);
    });
};

var deleteUser = function (req, res, next) {
  var user = req.user;
  profileDb
    .remove(user.id)
    .then(() => { res.json(user); })
    .catch((err) => {
      console.log("Delete failed", err);
      res.status(500);
      res.send(err);
    });
};

var getAllUsers = function (req, res, next) {
  profileDb
    .load({})
    .then((users) => {
      var output = JSON.stringify(Array.from(users), null, 2);
      output = output.replace(/_id/g, "id");
      res.send(output);
    })
    .catch((err) => {
      console.log(err);
      res.status(500);
      res.send(err);
    });
};

var getOneUser = function (req, res) {
  var user = req.user;
  if (user == null) {
    res.status(204).end();
  }
  else {
    var output = JSON.stringify(user, null, 2);
    //console.log(user);
    output = output.replace(/_id/g, "id");
    res.send(output);
  }
};

var setPassword = (req, res) => {
  var credentials = req.body;
  var record = {};
  var salt = crypto.pseudoRandomBytes(16);
  var password = crypto.pbkdf2(credentials.password, salt, 2000, 128, 'sha256', (err, derived_key) => {
    if (err) {
      res.status(500);
      res.json(createErrorResponse(err));
    }
    record._id = credentials.id;
    record.salt = salt.toString('hex');
    record.key = derived_key.toString('hex');
    passwordDb
      .save(record)
      .then(() => { res.json(createResponse("Password set", "User id: " + credentials.id)) })
      .catch((error) => {
        console.log("Could not save password", error);
        res.status(500);
        res.json(createErrorResponse(error));
      });
  });

}

var updatePassword = (req, res) => {
  var credentials = req.body;
  passwordDb
    .loadOne(credentials.id)
    .then((user) => {
      var salt = new Buffer(user.salt, 'hex');
      crypto.pbkdf2(credentials.old_password, salt, 2000, 128, 'sha256', (err, derived_key) => {
        if (derived_key.toString('hex') == user.key) {
          console.log("Password verified");
          //console.log("Credentials", credentials);
          setPassword(req, res);
        }
        else {
          // Password was incorrect
          res.status(400);
          res.json(createResponse("Authentication failed"));
        }
      });
    })
    .catch((error) => {
      res.status(500);
      res.json(createResponse("Authentication failed"));
    });
}

var deletePassword = (req, res) => {
  var credentials = req.body;
  passwordDb
    .remove(credentials.id)
    .then(() => { res.json(createResponse("Removed user", credentials.id)); })
    .catch((err) => {
      console.log("Delete failed", err);
      res.status(500);
      res.send(createErrorResponse(err));
    });

}

var authenticate = (req, res) => {
  var credentials = req.body;
  passwordDb
    .loadOne(credentials.id)
    .then((user) => {
      if (user) {
        var salt = new Buffer(user.salt, 'hex');
        var key = crypto.pbkdf2(credentials.password, salt, 2000, 128, 'sha256', (err, derived_key) => {
          //console.log("Derived key", derived_key.toString('hex'));
          //console.log("Stored key", user.key);
          if (derived_key.toString('hex') == user.key) {
            //console.log("Password verified");
            sign({ sub: credentials.id })
              .then((token) => { res.send(token) })
              .catch((error) => {
                console.log(error);
                res.status(500);
                res.json(createErrorResponse(error));
              });
          }
          else {
            // Password was incorrect
            res.status(400);
            res.json(createResponse("Authentication failed"));
          }
        });
      }
      else {
        // User not found
        res.status(400);
        res.json(createResponse("Authentication failed"));
      }
    })
    .catch((error) => {
      res.status(500);
      res.json(createErrorResponse("Authentication failed"));
    });
}

var validate = (req, res) => {
  var token = req.params.token;
  verify(token)
    .then((decoded) => {
      res.json(decoded);
    })
    .catch((error) => {
      res.json(createResponse("Verification failed", error));
    });
}

var getPubkey = (req, res) => {
  res.contentType('txt');
  res.send(pubk);
}

function createErrorResponse(title) {
  return createResponse(title, new Error().stack);
}

function createResponse(title, message) {
  var result = {};
  result.title = title;
  result.message = message;
  return result;
}

var getByIdUser = function (req, res, next, id) {
  profileDb
    .load({ '_id': id })
    .then((list) => {
      if (list.length > 0) {
        console.log(list);
        req.user = list[0];
        req.user.id = list[0]._id;
        delete req.user._id;
      }
      next();
    })
    .catch((err) => {
      res.status(500);
      res.send(err);
    });
};

router.route('/users')
  .post(createUser)
  .get(getAllUsers);

router.route('/users/:userId')
  .get(getOneUser)
  .put(updateUser)
  .delete(deleteUser);

router.param('userId', getByIdUser);

router.route('/auth/password')
  .post(setPassword)
  .put(updatePassword)
  .delete(deletePassword);

router.route('/auth/jwt')
  .post(authenticate);

router.route('/auth/jwt/:token')
  .get(validate);

router.route('/pubkey')
  .get(getPubkey);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1', router);

profileDb.connect();
passwordDb.connect();
app.listen(3000);

module.exports = app;
